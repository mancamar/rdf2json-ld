package it.cnr.isti.hiis.rdf2json_ld;

import com.github.jsonldjava.core.JsonLdOptions;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.github.jsonldjava.utils.JsonUtils;
import com.google.common.base.Charsets;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import org.apache.commons.io.FileUtils;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFHandlerException;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.StatementCollector;

/**
 *
 * @author Marco Manca
 */
public class transform {

//    public static void main(String[] args) {
//        try {
//            String rdfPath = System.getProperty("user.dir").concat(File.separator)
//                    .concat("src").concat(File.separator).concat("main").concat(File.separator)
//                    .concat("java").concat(File.separator).concat("it").concat(File.separator)
//                    .concat("cnr").concat(File.separator).concat("isti").concat(File.separator)
//                    .concat("hiis").concat(File.separator).concat("rdf2json_ld")
//                    .concat(File.separator).concat("waat_earl.xml");            
//            RDFParser parser = new TurtleR
//            RDFDataset parse = parser.parse(readLineByLineJava(rdfPath));
//            Object compact = JsonLdProcessor.fromRDF(parse);
//            System.out.println(JsonUtils.toPrettyString(compact));
//        } catch (IOException ex) {
//            Logger.getLogger(transform.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    
    private static String readLineByLineJava(String filePath) {
        StringBuilder contentBuilder = new StringBuilder();
        try (Stream<String> stream = Files.lines(Paths.get(filePath), StandardCharsets.UTF_8)) {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return contentBuilder.toString();
    }
    
     public static void main(String[] args) {
         String rdfPath = System.getProperty("user.dir").concat(File.separator)
                    .concat("src").concat(File.separator).concat("main").concat(File.separator)
                    .concat("java").concat(File.separator).concat("it").concat(File.separator)
                    .concat("cnr").concat(File.separator).concat("isti").concat(File.separator)
                    .concat("hiis").concat(File.separator).concat("rdf2json_ld");
                    //.concat(File.separator).concat("waat_earl.xml");            
        String filePath = rdfPath.concat(File.separator).concat("rdfGeneratedByMauve.xml");            
         try(InputStream in = new FileInputStream(filePath)){
            String jsonLD = getPrettyJsonLdString(in,RDFFormat.RDFXML);
            FileUtils.writeStringToFile(new File(rdfPath.concat(File.separator).concat("jsonLD.json")), 
                    jsonLD, Charset.defaultCharset());            
        }catch(Exception e){
            throw new RuntimeException(e);
        }
     }
     
     /**
     * @param in Input stream with rdf data
     * @param format format of the rdf data
     * @return a pretty JSON document as String
     */
    public static String getPrettyJsonLdString(InputStream in, RDFFormat format) {
        return getPrettyJsonLdString(
                readRdfToString(in, format, RDFFormat.JSONLD, ""));
    }

    /**
     * @param statements rdf statements collected
     * @return a pretty JSON document as String
     */
    public static String getPrettyJsonLdString(Collection<Statement> statements) {
        return getPrettyJsonLdString(
                graphToString(statements, RDFFormat.JSONLD));
    }

    private static String getPrettyJsonLdString(String rdfGraphAsJson) {
        try {
        //@formatter:off
                return JsonUtils
                        .toPrettyString(
                             cleanContext(
                                getFramedJson(
                                        createJsonObject(
                                                        rdfGraphAsJson))));
//                return JsonUtils.toPrettyString(getFramedJson(createJsonObject(rdfGraphAsJson)));
        //@formatter:on
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static Map<String, Object> removeGraphArray(Map<String, Object> framedJson) {
        List<Map<String,Object>> graph = (List<Map<String, Object>>) framedJson.get("@graph");
        return graph.get(0);
    }
    
    private static Map<String, Object> cleanContext(Map<String, Object> framedJson) {
        Map<String, Object> toRet = new HashMap<>();

        Map<String,String> contextMap = new HashMap<>(7);
        contextMap.put("mauve", "http://mauve.isti.cnr.it");
        contextMap.put("dcterms", "http://purl.org/dc/terms/");
        contextMap.put("ptr", "http://www.w3.org/2009/pointers#");
        contextMap.put("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        contextMap.put("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
        contextMap.put("xs", "http://www.w3.org/2001/XMLSchema");
        contextMap.put("xsd", "http://www.w3.org/2001/XMLSchema#");
        toRet.put("@context", contextMap);
        
//        List<Map<String,Object>> graph = (List<Map<String, Object>>) framedJson.get("@graph");
//        for(Map<String,Object> _tmp : graph) {
//            toRet.putAll(_tmp);
//        }
        toRet.put("@graph", framedJson.get("@graph"));
        return toRet;
    }

    private static Map<String, Object> getFramedJson(Object json) {
        try {
            return JsonLdProcessor.frame(json, getFrame(), new JsonLdOptions());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static Map<String, Object> getFrame() {
        Map<String, Object> context = new HashMap<>();
        context.put("@context", "http://schema.org/");
        return context;
    }

    private static Object createJsonObject(String ld) {
        try (InputStream inputStream =
                new ByteArrayInputStream(ld.getBytes(Charsets.UTF_8))) {
            Object jsonObject = JsonUtils.fromInputStream(inputStream);
            return jsonObject;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Collection<Statement> readRdfToGraph(
            final InputStream inputStream, final RDFFormat inf,
            final String baseUrl) {
        try {
            final RDFParser rdfParser = Rio.createParser(inf);
            final StatementCollector collector = new StatementCollector();
            rdfParser.setRDFHandler(collector);
            rdfParser.parse(inputStream, baseUrl);
            return collector.getStatements();
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String readRdfToString(InputStream in, RDFFormat inf,
            RDFFormat outf, String baseUrl) {
        Collection<Statement> myGraph = null;
        myGraph = readRdfToGraph(in, inf, baseUrl);
        return graphToString(myGraph, outf);
    }

    public static String graphToString(Collection<Statement> myGraph,
            RDFFormat outf) {
        StringWriter out = new StringWriter();
        RDFWriter writer = Rio.createWriter(outf, out);
        try {
            writer.startRDF();
            for (Statement st : myGraph) {
                writer.handleStatement(st);
            }
            writer.endRDF();
        } catch (RDFHandlerException e) {
            throw new RuntimeException(e);
        }
        return out.getBuffer().toString();
    }
}
